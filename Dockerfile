FROM openjdk:8-jdk-alpine
EXPOSE 4005
VOLUME /main-app
ADD /target/mailer-service-0.1.0.jar mailer-service-0.1.0.jar
ENTRYPOINT ["java","-jar","mailer-service-0.1.0.jar"]
package com.xlrs.mailer.view;

import java.util.List;

import com.xlrs.commons.view.BaseView;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseView implements BaseView{
	
	private static final long serialVersionUID = 1L;

	public ResponseView(String status){
		this.status = status;
	}
	private String status;
	
	private List<String> messages;
}

package com.xlrs.mailer.exception;


public class MailException extends Exception {
	
	private static final long serialVersionUID = -8006630353359097191L;
	protected String message;

	public MailException(String message, Exception e) {
		this.message = message;
	}
	
	public MailException(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

}

package com.xlrs.mailer.constant;

public class MailerConstants {
	
	public static String VIA_TLS 							= "TLS";
	public static String VIA_SSL 							= "SSL";
	public static String CONTENTTYPE_HTML 					= "HTML";
	public static String CONTENTTYPE_TEXT 					= "TEXT";
	public static String CONTENTTYPE_ATTACHMENT 			= "ATTACHMENT";

}

package com.xlrs.mailer.service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.xlrs.commons.view.EmailView;
import com.xlrs.security.security.JWTTokenUtility;
import com.xlrs.security.security.UserInContext;
import com.xlrs.security.util.ThreadAttribute;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailService {

	@Autowired(required = true)
	private JavaMailSender emailSender;

	@Value("${sender.mail.id}")
	private String senderMailId;
	
	@Value("${sender.name}")
	private String senderName;
	
	@Value("${spring.mail.support.id}")
	private String supportMailId;
	
	@Autowired
	JWTTokenUtility jwtTokenUtility;
	
	
	@Autowired
	private TemplateEngine templateEngine;
	
	public void sendMail(EmailView emailView) throws Exception {
		
		if(emailView.getRecipientMailId()==null && emailView.getRecipientUserId()!=null) {
			UserInContext uc = getUserInContext(emailView);
			if(uc!=null) {
				emailView.setRecipientMailId(uc.getEmail());
				if(emailView.getContentVariables()==null) {
					Map<String, Object> contentVariables = new HashMap<>();
					emailView.setContentVariables(contentVariables);
				}
				emailView.getContentVariables().put("recipientName", uc.getName());
			}
			if(emailView.getContentType()==null) {
				emailView.setContentType("HTML");
			}
		}
		sentMail(emailView);
	}

	@Async
	private void sentMail(EmailView emailView) throws  UnsupportedEncodingException, MessagingException {
		MimeMessage message = emailSender.createMimeMessage();
		message.setHeader("X-Priority", "2");
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

		Context context = new Context();
		context.setVariables(emailView.getContentVariables());
		String messageBody = templateEngine.process(emailView.getTemplateName(), context);
        helper.setBcc(supportMailId);
		helper.setTo(emailView.getRecipientMailId());
		helper.setText(messageBody, true);
		helper.setSubject(emailView.getSubject());
		helper.setFrom(senderMailId, senderName);
		log.info("sending mail to recipient id "+ emailView.getRecipientMailId());
		
		emailSender.send(message);
		log.info("e-mail sent to recipient id "+ emailView.getRecipientMailId());
	}

	private UserInContext getUserInContext(EmailView emailView) throws Exception {
		//Decode the JWT token and get the User In Context
		return  jwtTokenUtility.getUserContextFromToken(ThreadAttribute.getSecurityToken());
	}
	
}
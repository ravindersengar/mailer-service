package com.xlrs.mailer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableEurekaClient
@PropertySource({
	"classpath:messages.properties"
})
@ComponentScan(basePackages = "com.xlrs")
@EnableAsync
@EnableAdminServer
public class MailerApplication extends SpringBootServletInitializer{
	
	public static void main(String[] args) {
		SpringApplication.run(MailerApplication.class, args);
	}
}


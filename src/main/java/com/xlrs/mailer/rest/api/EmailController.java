package com.xlrs.mailer.rest.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.view.EmailView;
import com.xlrs.mailer.service.EmailService;

@RestController
@RequestMapping(value = "/mailer")
public class EmailController {

	@Autowired
	private EmailService emailService;
	
	@Autowired
	private MessageSource messages;
	
	@PostMapping("/email")
	public void sendMail(@RequestBody EmailView email) throws Exception {
		try {
			emailService.sendMail(email);
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.send.mail", null, null), e);
		}
	}
}
